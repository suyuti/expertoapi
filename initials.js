const Urun = require("./models2/urun");
const Sektor = require("./models2/sektor");
const Board = require('./models2/board');
const seedData = require('./seedData.json');

async function initUrunler() {
    for (var i = 0; i < seedData.Urunler.length; ++i) {
        let u = seedData.Urunler[i]
        let _u = await Urun.findOne({Adi: u.Adi}) 
        if (!_u) {
            var urun = new Urun(u)
            await urun.save()
        }
    }
}

async function initSektor() {
    var sektorler = [
        "Kimya",
        "Makine",
        "Tekstil",
        "Bilişim"
    ]

    for (var i = 0; i < sektorler.length; ++i) {
        var _s = sektorler[i]
        var sektor = await Sektor.findOne({Adi: _s})
        if (!sektor) {
            sektor = new Sektor({Adi: _s})
            await sektor.save()
        }
    }
}

async function initSatisBoard() {
    var sb = await Board.findOne({Adi: "Satış Fırsatları"})
    if (!sb) {
        var satisBoard = new Board({
            name: 'Satış Fırsatları',
            uri: 'Satis',
            settings: {
                subscribed: false
            },
            lists: [
                {name: "Ön Toplantı"},
                {name: "Teknik Toplantı"}
            ]
        })
        await satisBoard.save()
    }
}

exports.initializeDb = async () => {
    initSektor()
    initSatisBoard()
    initUrunler()
}
