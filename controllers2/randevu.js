const Randevu = require("../models2/randevu");

exports.list = async (req, res) => {
    console.log('randevu get list')
    const randevular = await Randevu.find()
    return res.status(200).json(randevular)
}

exports.getById = async (req, res) => {
    console.log('randevu getbyid')
    const randevu = await Randevu.findOne({_id: req.params.id})
    return res.status(200).json(randevu)
}

exports.create = async (req, res) => {
    console.log('randevu create')
    let payload = req.body;
    var r = {...payload}
    r.FirmaKatilimcilar = payload.FirmaKatilimcilar.map(k => k.value)
    r.ExpertoKatilimcilar = payload.ExpertoKatilimcilar.map(k => k.value)
    var randevu = new Randevu(r)
    let row = await randevu.save()
    return res.status(200).json(row)
}
