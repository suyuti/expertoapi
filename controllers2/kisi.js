const Musteri   = require("../models2/musteri");
const Kisi      = require("../models2/kisi");

exports.listAll = async (req, res) => {
    console.log('Kisiler list')
    const kisiler = await Kisi.find({}).populate('Musteri', 'Markasi')
    if (kisiler) {
        return res.status(200).json(kisiler)
    }
}

exports.list = async (req, res) => {
    console.log('Musteri kisiler list')
    const musteri = await Musteri.findById(req.params.musteriId)
    console.log(musteri)
    if (musteri) {
        const kisiler = await Kisi.find({Musteri: musteri._id}).populate('Musteri', 'Markasi');
        console.log(kisiler)
        if (kisiler) {
            return res.status(200).json(kisiler)
        }
    }

    return res.status(200).json(null)
}
  
exports.create = async (req, res) => {
    console.log('Kisi olustur')
    let payload = req.body;
    var musteri = await Musteri.findById(req.params.musteriId)
    if (!musteri) {
        return res.status(200).json(null)
    }
    var kisi = new Kisi(payload)
    kisi.Musteri= musteri
    let row = await kisi.save()
    return res.status(200).json(row)
}

exports.getById = async (req, res) => {
    console.log('Kisi getir')
    console.log(req.params.kid)
    const kisi = await Kisi.findOne({_id: req.params.kid}).populate('Musteri', 'Markasi')
    return res.status(200).json(kisi)
}

exports.update = async (req, res) => {
    console.log('Kisi guncelle')

    Kisi.findByIdAndUpdate(req.params.id, req.body, {new:true},
        (err, doc) => {
            if (err) {
                return res.status(404).json(null)
            }
            else {
                return res.status(200).json(doc)
            }
        })
}

exports.remove = async (req, res) => {
    console.log('Kisi sil')
    Kisi.remove({_id: req.query.id}, (e) => {
        if (e) {
            return res.status(404).json(err)
        }
        else {
            return res.status(200).json('')
        }
    })
}

