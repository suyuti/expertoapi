const {Task, Label, Filter} = require('../models2/task')

exports.list = async (req, res) => {
    console.log(req.query)
    var query = {}
    if (req.query.filterHandle) {
        query[req.query.filterHandle] = true
        let tasks = await Task.find(query)
        console.log(tasks)
        return res.status(200).json(tasks)
        }
    if (req.query.labelHandle) {
        query = {
            Labels: req.query.labelHandle
        }
        console.log(query)
        let tasks = await Task.find(query)
        console.log(tasks)
        return res.status(200).json(tasks)
    }
}

exports.update = async (req, res) => {
    console.log(req.body)
    var task = {...req.body}
    delete task._id
    Task.findByIdAndUpdate(req.body._id, task, {new:true},
        (err, doc) => {
            if (err) {
                return res.status(404).json(null)
            }
            else {
                return res.status(200).json(doc)
            }
        })
}

exports.create = async (req, res) => {
    const task = req.body
    console.log(task)
    var newTask = new Task({
        Baslik      : task.Baslik,
        Aciklama    : task.Aciklama,
        StartDate   : task.StartDate,
        DueDate     : task.DueDate,
        Completed   : task.Completed,
        Starred     : task.Starred,
        Important   : task.Important,
        Deleted     : task.Deleted,
//        Labels      :
    
    })
    let row = await newTask.save()
    return res.status(200).json(row)
}

exports.remove = async (req, res) => {
    return res.status(200).json(null)
}

exports.filters = async (req, res) => {
    let filters = await Filter.find()
    return res.status(200).json(filters)
}

exports.labels = async (req, res) => {
    let labels = await Label.find()
    return res.status(200).json(labels)
}

exports.folders = async (req, res) => {
    return res.status(200).json([])
}

exports.setFolder = async (req, res) => {
    return res.status(200).json(null)
}

exports.toggleLabel = async (req, res) => {
    return res.status(200).json(null)
}

exports.deleteTodos = async (req, res) => {
    return res.status(200).json(null)
}



