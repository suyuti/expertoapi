const SatisFirsati = require('../models2/satisFirsati')

exports.list = async (req, res) => {
    console.log('sf list')
    if (req.query.filter) {
        console.log(req.query.filter)
        if (req.query.filter === "onaybekleyen") {
            const sfs = await SatisFirsati.find({Onay: false});
            return res.status(200).json(sfs)
        }
    }
    const sfs = await SatisFirsati.find()
        .populate("Musteri", "Markasi")
        .populate("OlusturanKisi", "UserName")
        
    return res.status(200).json(sfs)
}

exports.getById = async (req, res) => {
    console.log('sf getById')
    const sf = await SatisFirsati.findById(req.params.id)
        .populate("Musteri", "Markasi")
        .populate("OlusturanKisi", "UserName")

    return res.status(200).json(sf)
}

exports.create = async (req, res) => {
    console.log('sf create')
    console.log(req.body)
    let payload = req.body;
    var sfs = new SatisFirsati(payload);
    let row = await sfs.save()
    return res.status(200).json(row)
    //return res.status(200).json('')
}

exports.widgetdata = async (req, res) => {
    console.log('sf widget data')
    var userId = req.params.userId
    let sf = await SatisFirsati.find({OlusturanKisi: userId})
    //var toplam = 0
    //for (var i = 0; i < sf.length && i < 7; ++i) {
    //    toplam += go[i].Adet
    //}

    var result = {
        sf: {
            value: sf.length,
            ofTarget: 20
        },
        chartType: 'bar',
        datasets  : [
            {
                label: 'Conversion',
                data : [221, 428, 492, 471, 413, 344, 294]
            }
        ],
        labels    : ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
        options   : {
            spanGaps           : false,
            legend             : {
                display: false
            },
            maintainAspectRatio: false,
            layout             : {
                padding: {
                    top   : 24,
                    left  : 16,
                    right : 16,
                    bottom: 16
                }
            },
            scales             : {
                xAxes: [
                    {
                        display: false
                    }
                ],
                yAxes: [
                    {
                        display: false,
                        ticks  : {
                            min: 100,
                            max: 500
                        }
                    }
                ]
            }
        }
    }
    return res.status(200).json(result)
}
