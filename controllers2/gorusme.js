const moment = require('moment')
const {Gorusme, GorusmeOzet} = require("../models2/gorusme");

exports.list = async (req, res) => {
    console.log('get gorusme list')
    var ObjectId = require('mongoose').Types.ObjectId; 
    const gorusmeler = await Gorusme.find({Musteri: new ObjectId(req.params.musteriId)})
        .populate('Musteri', 'Markasi')
        .populate('GorusmeYapilanKisi', 'Adi')
        .populate('GorusmeyiYapanKisi', 'UserName')
        .sort('Zaman')
    return res.status(200).json(gorusmeler)
}
  
exports.create = async (req, res) => {
    let payload = req.body;
    var gorusme = new Gorusme(payload)
    let row = await gorusme.save()
    const zaman = moment(payload.Zaman).format("DD/MM/YYYY")

    if (row) {
        var go = await GorusmeOzet.findOne({GorusmeYapan: gorusme.GorusmeyiYapanKisi._id, Tarih: zaman})
        if (go) {
            go.Adet = go.Adet + 1
            await go.save()
        }
        else {
            let go = new GorusmeOzet({
                GorusmeYapan: gorusme.GorusmeyiYapanKisi._id,
                Tarih: zaman,
                Adet: 1
            })
            await go.save()
        }
    }

    return res.status(200).json(row)
}

exports.getById = async (req, res) => {
    console.log('gorusme get by id')
    const gorusme = await Gorusme.find({_id: req.params.gid})
    return res.status(200).json(gorusme[0])
}

exports.remove = async (req, res) => {
    console.log('gorusme remove')
    Gorusme.remove({_id: req.params.gid}, (e) => {
        if (e) {
            return res.status(404).json(err)
        }
        else {
            return res.status(200).json('')
        }
    })
}

exports.widgetdata = async (req, res) => {
    console.log('gorusme widget data')
    var userId = req.params.userId
    let go = await GorusmeOzet.find({GorusmeYapan: userId})//.order('Tarih')
    var toplam = 0
    for (var i = 0; i < go.length && i < 7; ++i) {
        toplam += go[i].Adet
    }

    var result = {
        gorusme: {
            value: toplam,
            ofTarget: 20
        },
        chartType: 'bar',
        datasets  : [
            {
                label: 'Conversion',
                data : [221, 428, 492, 471, 413, 344, 294]
            }
        ],
        labels    : ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
        options   : {
            spanGaps           : false,
            legend             : {
                display: false
            },
            maintainAspectRatio: false,
            layout             : {
                padding: {
                    top   : 24,
                    left  : 16,
                    right : 16,
                    bottom: 16
                }
            },
            scales             : {
                xAxes: [
                    {
                        display: false
                    }
                ],
                yAxes: [
                    {
                        display: false,
                        ticks  : {
                            min: 100,
                            max: 500
                        }
                    }
                ]
            }
        }
    }
    return res.status(200).json(result)
}