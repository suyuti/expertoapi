var _ = require('lodash');
const jwt       = require('jsonwebtoken');
const config    = require('../config');
const userModel = require("../models2/user");
const command   = require("../command");


exports.auth = async (req, res) => {
    console.log('auth')
    const data = req.body.data;
    const {email, password} = data;
    const _user = await userModel.findOne( {EMail : email, Active: true})    
    
    const user = _.cloneDeep(_user);
    let verified =false
    user ? verified= command.VerifyHash(password, user.Password):"";

    const error = {
        email   : user ? null : 'Check your username/email',
        password: user && verified ? null : 'Check your password'
    };

    if ( !error.email && !error.password && !error.displayName )
    {
        var u = user.toObject()
        u.role = u.Role
        delete u.Role
        delete u.Password

        const access_token = jwt.sign({id: user._id}, config.secret, {expiresIn: config.expiresIn});

        const response = {
            "user"        : u,
            "access_token": access_token
        };

        return res.status(200).json(response);
    }
    else
    {
        return res.status(200).json(error);
    }
}


exports.access_token = async (req, res) => {
    console.log('access_token')
    const data = req.body.data;
    const {access_token} = data;

    try
    {
        const {id} = jwt.verify(access_token, config.secret);
        var _user = await userModel.findById(id)
        if(!_user){
            const errorUser = "User not Found Or not Activeted";
            return res.status(200).json(error)
        }
       
        const user = _.cloneDeep(_user);
        var u = user.toObject()
        u.role = u.Role
        delete u.Password
        delete u.Role

        const updatedAccessToken = jwt.sign({id: user._id}, config.secret, {expiresIn: config.expiresIn});

        const response = {
            "user"        : u,
            "access_token": updatedAccessToken
        };

        return res.status(200).json(response)
    } catch ( e )
    {
        const error = "Invalid access token detected";
        return res.status(200).json(error)
    }
}

exports.register = async (req, res) => {
    console.log('register')
    const data = req.body
    const {displayName, password, email} = data;
    const isEmailExists = await userModel.findOne({EMail : email});
    const error = {
        email      : isEmailExists ? 'The email is already in use' : null,
        displayName: displayName !== '' ? null : 'Enter display name',
        password   : null
    };
    if ( !error.displayName && !error.password && !error.email )
    {

        var pass = command.HashPassword(password)
        var newUser = new userModel({
            FirstName   : '',
            LastName    : '',
            UserName    : displayName,
            EMail       : email,
            Password    : pass,
            data    : {
                displayName: displayName,
                photoURL   : 'assets/images/avatars/Abbott.jpg',
                email      : email,
                settings   : {},
                shortcuts  : []
            }
        })
        var _newUser = await newUser.save()
        const user = _.cloneDeep(_newUser);
        var u = user.toObject()
        u.role = u.Role
        delete u.Role
        delete u.Password

        const access_token = jwt.sign({id: user._id}, config.secret, {expiresIn: config.expiresIn});

        const response = {
            "user"        : u,
            "access_token": access_token
        };

        return res.status(200).json(response);
    }
    else
    {
        return res.status(200).json(error);
    }
}
exports.update = async (req, res) => {}
