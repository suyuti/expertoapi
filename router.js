var express = require('express');
var router = express.Router();  
const danismanController = require("./controllers/danismanController"); 
const desteklerController = require("./controllers/desteklerController"); 
const gorusmeKayitlariController = require("./controllers/gorusmeKayitlariController"); 
const kisiController = require("./controllers/kisiController"); 
const musteriController = require("./controllers/musteriController"); 
const musteriKaynakController = require("./controllers/musteriKaynakController"); 
const OSBController = require("./controllers/OSBController"); 
const sektorController = require("./controllers/sektorController"); 
const sozlesmeController = require("./controllers/sozlesmeController"); 
const teklifController = require("./controllers/teklifController"); 
const urunController = require("./controllers/urunController"); 
const userController = require("./controllers/userController"); 
const loginController = require("./controllers/loginController"); 
const ilController = require("./controllers/ilController"); 
const widgetsController = require("./controllers/widgetsController"); 
const authController = require("./controllers/authContrller"); 
const satisFirsatiContrller = require("./controllers/satisFirsatiContrller"); 
/*
const AuthController = require('./controllers2/auth')
const UserController = require('./controllers2/user')
const MusteriController = require('./controllers2/musteri')

router.post('/auth/login',      AuthController.loginUser)
router.post('/auth/register',   AuthController.registerUser)

router.get('/user/list',        UserController.list)
router.delete('/user',      UserController.remove)
router.put('/user',      UserController.update)

router.get('/musteri/list',        MusteriController.list)
*/


router.get      ('/musteri/potansiyel/list',    musteriController.potansiyelMusteriler)
router.get      ('/musteri/aktif/list',         musteriController.aktifMusteriler)
router.get      ('/musteri/pasif/list',         musteriController.pasifMusteriler)
router.get      ('/musteri/sorunlu/list',       musteriController.sorunluMusteriler)


router.get      ('/musteri',                          musteriController.list)     
router.post     ('/musteri/save',                     musteriController.create)  
router.get      ('/musteri/:id',                      musteriController.getById)        
router.put      ('/musteri',                          musteriController.update)     
router.delete   ('/musteri/:id',                      musteriController.delete) 
router.get      ('/musteri/:id/teklifler',            musteriController.getTeklifler)   
router.get      ('/musteri/:id/teklifler/onaybekleyen',            musteriController.getOnayBekleyenTeklifler)   
router.get      ('/musteri/:id/destek/history',         musteriController.getGecmisDestek)             



router.get      ('/satisFirsati',                       satisFirsatiContrller.list)     
router.post     ('/satisFirsati',                       satisFirsatiContrller.create)         
router.post     ('/satisFirsati/:id/gorusmeKayitlari',  satisFirsatiContrller.addGorusmeKayitlari)  
router.post     ('/satisFirsati/:id/randevu',           satisFirsatiContrller.addRandevu)  
router.post     ('/satisFirsati/:id/teklif',            satisFirsatiContrller.addTeklif)  
router.get      ('/satisFirsati/:id/kisiler',            satisFirsatiContrller.getSatisFirsatiFirmasininKisileri)  


router.put      ('/satisFirsati/:id',               satisFirsatiContrller.update)     
router.delete   ('/satisFirsati/:id',               satisFirsatiContrller.delete)   

router.get      ('/danisman',                   danismanController.list)     
router.post     ('/danisman',                   danismanController.create)         
router.put      ('/danisman/:id',               danismanController.update)     
router.delete   ('/danisman/:id',               danismanController.delete)           



router.get      ('/destek',                             desteklerController.list)     
router.post     ('/destek',                             desteklerController.create)          
router.put      ('/destek/:id',                         desteklerController.update)     
router.delete   ('/destek/:id',                         desteklerController.delete)              



router.get      ('/gorusmeKayit',                           gorusmeKayitlariController.list)     
router.post     ('/gorusmeKayit',                           gorusmeKayitlariController.create)  
 router.put     ('/gorusmeKayit/:id',                      gorusmeKayitlariController.update)     
router.delete   ('/gorusmeKayit/:id',                       gorusmeKayitlariController.delete)            



router.get      ('/kisi',                           kisiController.list)     
router.post     ('/kisi',                           kisiController.create)         
router.put      ('/kisi/:id',                       kisiController.update)     
router.delete   ('/kisi/:id',                       kisiController.delete)            






router.get      ('/musteriKaynak',                       musteriKaynakController.list)     
router.post     ('/musteriKaynak',                          musteriKaynakController.create)  
router.get      ('/musteriKaynak/:id',                      musteriKaynakController.getById)        
router.put      ('/musteriKaynak/:id',                      musteriKaynakController.update)     
router.delete   ('/musteriKaynak/:id',                      musteriKaynakController.delete)            



router.get      ('/osb',                       OSBController.list)     
router.post     ('/osb',                          OSBController.create)  
router.get      ('/osb/:id',                      OSBController.getById)        
router.put      ('/osb/:id',                      OSBController.update)     
router.delete   ('/osb/:id',                      OSBController.delete)            



router.get      ('/sektor',                       sektorController.list)     
router.post     ('/sektor',                          sektorController.create)  
router.get      ('/sektor/:id',                      sektorController.getById)        
router.put      ('/sektor/:id',                      sektorController.update)     
router.delete   ('/sektor/:id',                      sektorController.delete)            



router.get      ('/sozlesme',                       sozlesmeController.list)     
router.post     ('/sozlesme',                          sozlesmeController.create)  
router.get      ('/sozlesme/:id',                      sozlesmeController.getById)        
router.put      ('/sozlesme/:id',                      sozlesmeController.update)     
router.delete   ('/sozlesme/:id',                      sozlesmeController.delete)            



router.get      ('/teklif',                             teklifController.list)     
router.post     ('/teklif',                             teklifController.create)  
router.get      ('/teklif/:id',                         teklifController.getById)        
router.put      ('/teklif',                             teklifController.update)     
router.delete   ('/teklif/:id',                         teklifController.delete)   
router.put      ('/teklif/:id/onayiste',                teklifController.onayiste)           
router.put      ('/teklif/:id/OnayVer',                 teklifController.onayVer)  
router.put      ('/teklif/:id/remove',                  teklifController.delete)          
router.put      ('/teklif/:id/state/:state',            teklifController.state)   


router.get      ('/urun',                       urunController.list)     
router.post     ('/urun',                          urunController.create)  
router.get      ('/urun/:id',                      urunController.getById)        
router.put      ('/urun/:id',                      urunController.update)     
router.delete   ('/urun/:id',                      urunController.delete)                 



router.get      ('/user',                           userController.list)     
router.post     ('/user',                           userController.create)  
router.get      ('/user/:id',                       userController.getById)        
router.put      ('/user/updateUserByAdmin',         userController.updateUserByAdmin)  
router.put      ('/user/:id',                       userController.update)     
router.delete   ('/user/:id',                       userController.delete) 





router.get      ('/iller',                           ilController.list)   
router.get      ('/iller/:id',                       ilController.ilce)   



router.get      ('/userast',                       userController.getAllAstUserList)   

   
router.post      ('/login',                       loginController.login)    
router.post      ('/forgotPassword',              loginController.forgotPassword)    
router.post      ('/forgotPasswordChange',        loginController.forgotPasswordChange)    
router.get      ('/hash/:key',                    loginController.createHash)   


//router.post      ('/auth', userController.auth)
//router.post      ('/auth/access-token', userController.accessToken)

router.get      ('/widgets/2',                          widgetsController.widget2)
router.get      ('/widgets/teklifler',                  widgetsController.widgetTeklifler)
router.get      ('/widgets/onaybekleyenteklifler',      widgetsController.widgetOnayBekleyenTeklifler)
router.get      ('/widgets/widgetMusteriAdaylariData',  musteriController.getWidgetMusteriAdaylariData)             



router.post      ('/auth', authController.auth)
router.post      ('/auth/access-token', authController.access_token)
router.post      ('/auth/register', authController.register)





router.get      ('/deneme', (req, res)=>{ return "jiwstmail"})   


module.exports = router;
