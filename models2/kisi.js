const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
    Musteri     : { type: Schema.Types.ObjectId, ref: 'Musteri', required: true },
    Adi         : { type: String,  required: true },
    Soyadi      : { type: String,  required: true },
    Unvani      : { type: String,  required: false },
    Cinsiyet    : { type: String,  enum: ['Erkek', 'Kadın'], default: 'Erkek' },
    Mail        : { type: String,  required: false },
    CepTelefonu : { type: String,  required: false },
    IsTelefonu  : { type: String,  required: false },
    Dahili      : { type: String,  required: false },
    TCKN        : { type: String,  required: false },
    DogumTarihi : { type: String,  required: false },
    Yetki       : { type: String,  enum : ['Yönetim Yetkilisi', 'Teknik Yetkilisi', 'Mali Yetkilisi' ], required: false },
},{ 
  versionKey: false 
});

module.exports = mongoose.model("Kisi", SchemaModel);
