const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var GorusmeOzetSchemaModel = new Schema({ 
  GorusmeYapan: {type: Schema.Types.ObjectId, ref: 'User2'},
  Tarih       : String,
  Adet        : Number
},{ 
  versionKey: false 
});

var SchemaModel = new Schema({ 
  Musteri             : { type: Schema.Types.ObjectId, ref: 'Musteri', required: true },
  GorusmeyiYapanKisi  : { type: Schema.Types.ObjectId, ref: 'User2', required: true },
  GorusmeYapilanKisi  : { type: Schema.Types.ObjectId, ref: 'Kisi', required: true },
  Zaman               : { type: Date, required: true },
  Kanal               : { type: String, enum: ['Telefon', 'Mail', 'Yüzyüze', 'Whatsapp'],  required: true, default: 'Telefon' },
  Konu                : { type: String,  required: true },
  Aciklama            : { type: String },
},{ 
versionKey: false 
});

module.exports = {
  Gorusme: mongoose.model("Gorusme", SchemaModel),
  GorusmeOzet: mongoose.model("GorusmeOzet", GorusmeOzetSchemaModel),
}
