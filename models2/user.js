const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
    FirstName     : { type: String,  required: false },
    LastName      : { type: String,  required: false },
    UserName      : { type: String,  required: false },
    EMail         : { type: String,  required: true  },
    Password      : { type: String,  required: true  },
    Active        : { type: Boolean, default: false  },
    Role          : { type: String,  default: 'gu' }, // Guest
    HomePage      : { type: String,  default: '/home'},
    data : {
        displayName: {type: String, default: ''},        //?
        photoURL   : {type: String, default: ''},
        email      : {type: String, default: ''},        //?
        settings     : {
            layout   : {
                style : {type: String, default: 'layout1'},
                config: {
                    scroll : {type: String, default: 'content'},
                    navbar : {
                        display : {type: Boolean, default: true},
                        folded  : {type: Boolean, default: false},
                        position: {type: String, default: 'left'},
                    },
                    toolbar: {
                      display : {type: Boolean, default: true},
                      style  : {type: String, default: 'fixed'},
                      position: {type: String, default: 'below'},
                  },
                    footer : {
                      display : {type: Boolean, default: true},
                      style  : {type: String, default: 'fixed'},
                      position: {type: String, default: 'below'},
                    },
                    mode   : {type: String, default: 'fullWidth'}
                }
            },
            customScrollbars: {type: Boolean, default: true},
            theme           : {
                main   : {type: String, default: 'default'},
                navbar : {type: String, default: 'default'},
                toolbar: {type: String, default: 'default'},
                footer : {type: String, default: 'default'}
            }
        },
        shortcuts    : [String]
    }
},{ 
  versionKey: false 
});


module.exports = mongoose.model("User2", SchemaModel);
