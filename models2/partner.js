const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({
    Adi             : { type: String, required: true },
    Unvani          : { type: String, required: false },
    VergiDairesi    : { type: String, required: false },
    VergiNo         : { type: String, required: false },
    Telefon         : { type: String, required: false },
    Mail            : { type: String, required: false },
    KomisyonOrani   : { type: String, required: true, default: 0 },
    Aktif           : { type: Boolean, default: true }
},{ 
  versionKey: false 
});


module.exports = mongoose.model("Partner", SchemaModel);
