const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
  Musteri   : { type: String,  required: true },
  Tip       : { type: String, enum : ["Reklam", "Mail", "İç Personel Yönlendirme", "Partner", "Refrans", "Web", "Fuar"], required: true},
  Data : {
    ReklamData: {
      ReklamTarihi: Date,
      ReklamAciklama: String
    },
    MailData: {
      MailTarihi: Date,
      MailBaslik: String,
      MailAciklama: String
    },
    IcPersonelData: {
      Kim: {type: Schema.Types.ObjectId, ref: "User"},
      Aciklama: String
    },
    PartnerData: {
      Partner: { type: Schema.Types.ObjectId, ref: "Partner"},
      Oran: Number,
      Tarih: Date,
      Aciklama: String
    },
    ReferansData: {
      Musteri: {type: Schema.Types.ObjectId, ref: "Musteri"},
      Tarih: Date,
      Aciklama: String
    },
    WebData: {
      Tarih: Date,
      Aciklama: String
    },
    FuarData: {
      HangiFuar: String,
      Tarih: Date,
      Aciklama: String
    }
  }
},{ 
versionKey: false 
});

module.exports = mongoose.model("MusteriKaynak", SchemaModel);
