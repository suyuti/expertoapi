const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
    name             : { type: String},
    uri             : { type: String},
    settings        : {
        color       : { type: String},
        subscribed  : { type: String},
        cardCoverImages: { type: Boolean, default: false},
    },

    lists: [{
        name : String,
        idCards: [{type: Schema.Types.ObjectId, ref: 'SatisFirsati'}]
    }],
    cards: [{
        name: String,
        description: String, 
        idAttachmentCover: String,
        idMembers: [String],
        idLabels: [String],
        attachments: [String],
        subscribed: Boolean,
        checklists: [String],
        due: Date
    }],
    members: [{
        name: String,
        avatar: String
    }],
    labels: [{
        name: String,
        class: String
    }]
},{ 
  versionKey: false 
});

module.exports = mongoose.model("SatisBoard", SchemaModel);
