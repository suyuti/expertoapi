const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var TaskFilterSchemaModel = new Schema({ 
    Handle      : { type: String },
    Title       : { type: String },
    Color       : { type: String },
},{ 
  versionKey: false 
});

var TaskFolderSchemaModel = new Schema({ 
    Handle      : { type: String },
    Title       : { type: String },
    Color       : { type: String },
},{ 
  versionKey: false 
});

var TaskLabelSchemaModel = new Schema({ 
    Handle      : { type: String },
    Title       : { type: String },
    Color       : { type: String },
},{ 
  versionKey: false 
});

var TaskSchemaModel = new Schema({ 
    Baslik      : { type: String,  required: true },
    Aciklama    : { type: String,  required: true },
    StartDate   : { type: Date },
    DueDate     : { type: Date },
    Completed   : { type: Boolean,  default: false },
    Starred     : { type: Boolean,  default: false },
    Important   : { type: Boolean,  default: false },
    Deleted     : { type: Boolean,  default: false },
    Labels      : [{ type: Schema.Types.ObjectId, ref: 'TaskLabel' }]
},{ 
  versionKey: false 
});

module.exports = {
    Task    : mongoose.model("Task", TaskSchemaModel),
    Label   : mongoose.model("TaskLabel", TaskLabelSchemaModel),
    Folder  : mongoose.model("TaskFolder", TaskFolderSchemaModel),
    Filter  : mongoose.model("TaskFilter", TaskFilterSchemaModel)
}
