const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
    Musteri             : { type: Schema.Types.ObjectId, ref: 'Musteri', required: true },
    OlusturanKisi       : { type: Schema.Types.ObjectId, ref: 'User2', required: true },
    OlusturmaZamani     : { type: Date, default: new Date()},
    Adi                 : { type: String,  required: true },
    Aciklama            : { type: String },
    SeciliUrunler       : [
      {
        Urun                    : { type: Schema.Types.ObjectId, ref: 'Urun'},
        UrunAdi                 : String,
        OnOdemeTutari           : { type: Number },
        YeniOnOdemeTutari       : { type: Number },
        RaporBasiOdemeTutari    : { type: Number },
        YeniRaporBasiOdemeTutari: { type: Number },
        YuzdeTutari             : { type: Number },
        YeniYuzdeTutari         : { type: Number },
        SabitTutar              : { type: Number },
        YeniSabitTutar          : { type: Number },
        BasariTutari            : { type: Number },
        YeniBasariTutari        : { type: Number },
        FiyatDegisikligiVar     : { type: Boolean, default: false}
      }],
    AraToplam           : Number,
    Indirim             : Number,
    KDV                 : Number,
    Toplam              : Number,
    Onay                : Boolean,
    log                 : [ String ]
},{ 
  versionKey: false 
});

module.exports = mongoose.model("SatisFirsati", SchemaModel);
