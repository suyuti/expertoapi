const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema({ 
  SatisFirsati        : { type: Schema.Types.ObjectId, ref: 'SatisFirsati' },
  Musteri             : { type: Schema.Types.ObjectId, ref: 'Musteri', required: true },
  MusteriAdi          : String,
  FirmaKatilimcilar   : [{ type: Schema.Types.ObjectId, ref: 'Kisi' }],
  ExpertoKatilimcilar : [{ type: Schema.Types.ObjectId, ref: 'User2', required: true }],
  Tarih               : { type: Date, required: true },
  Yer                 : { type: String },
  Konu                : { type: String,  required: true },
  Aciklama            : { type: String },
  Durum               : { type: String, enum: ['Planlandi', 'Yapildi', 'Iptal'], default: 'Planlandi'},
  Olusturan           : { type: Schema.Types.ObjectId, ref: 'User2'}
},{ 
versionKey: false 
});

module.exports = mongoose.model("Randevu", SchemaModel)
