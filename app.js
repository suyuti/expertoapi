
const express       = require('express');
const app           = express();
const cors          = require('cors');
const bodyParser    = require('body-parser');
const command       = require('./command')
var ejs             = require('ejs');
const initializer   = require('./initials')

require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })
require("./config/db");

console.clear();
console.log("Mode: " + process.env.NODE_ENV)

initializer.initializeDb()

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '100mb' }));
app.use(cors());
app.use((req, res, next)=> {console.clear();next() });

// geri dondurulecek degeri otomatik yapacak
app.use(command.ResponseModify);
app.use(command.TokenKontrol);

// API ENDPOINTS
//app.use('/api', require("./router"))
app.use('/api', require("./router2"))
/*
app.get('/sendMail', async function (req, res) {
    var data = {
        "VerilisTarihi": "2020-01-27T14:45:33.074Z",
        "TeklifDurumu": 10,
        "Loglar": [
            {
                "date": "2020-01-27T14:51:04.209Z",
                "description": "5e2ef87797444a32e038e987 li kişi tarafından oluşturulmuştur"
            },
            {
                "date": "2020-01-27T14:51:31.591Z",
                "description": "5e2ef87797444a32e038e987 li kişi teklifin durumunu 1 olarak guncelledi"
            },
            {
                "date": "2020-01-27T14:56:28.670Z",
                "description": "5e15c066ecc56c06e0ae9167 li kişi teklifin durumunu 10 olarak guncelledi"
            }
        ],
        "_id": "5e25b9088d40fc1b1c908e10",
        "Firma": {
            "OncekiDestekler": [],
            "_id": "5e25b9088d40fc1b1c908e10",
            "FirmaUnvani": "smilecetin",
            "il": "Kastamonu",
            "ilce": "Tosya",
            "Telefon": "1456456456456",
            "Status": 0,
            "Adres": "nhıghıghıhjıhjıhjıhjıhjı"
        },
        "MusteriTemsilcisi": {
            "Ast": [],
            "_id": "5e2ef87797444a32e038e987",
            "Adi": "ismail2",
            "KullaniciAdi": "ismail2",
            "Password": "a722d73fdff2aae6ca925101ba7b4b95$c8d52cbba1a7deb2915311c41bd4482d5b73a91fc2700ba2d952de35c3ea3f2c",
            "Email": "ismail2@ee.com",
            "Ust": "5e15c066ecc56c06e0ae9167",
            "IndirimOrani": 10
        },
        "IlgiliKisi": {
            "_id": "5e26de378d40fc1b1c908e25",
            "Adi": "ismail",
            "Unvani": "idd",
            "CepTel": "5313211111",
            "Mail": "iidsif",
            "IsTel": "5313211111",
            "Dahili": "55665323223",
            "Cinsiyet": "Kadın",
            "Type": 1
        },
        "OnOdemeTutari": 20000,
        "SabitTutar": 4000,
        "BasariTutari": 20000,
        "Urun": {
            "AktifMi": true,
            "Dashboard": 0,
            "_id": "5e2574a26e5b69540ba5ee7d",
            "Adi": "Ar-Ge Merkezi Kurulum",
            "OnOdemeTutari": 20000,
            "BasariTutari": 20000,
            "SabitTutar": 4000,
            "KDVOrani": 18
        },
        "KapanisTarihi": "2020-01-27T21:00:00.000Z",
        "IadeliMi": true,
        "SartlarVeKosullar": "fd",
        "Onaylayan": "5e15c066ecc56c06e0ae9167"
    }

    const html = await ejs.renderFile('./Views/teklifOnayTalebi.ejs', {
        title: '[TEKLIF_ONAY_TALEBI]',
        data: data
    })
    // res.send(html)
    var to = "ismail.cetin@experto.com.tr"
    var subject = "[TEKLIF_ONAY_TALEBI]"
    command.sendMail(to, subject, html);
    res.send({ html: html })
})
*/

/*
app.get('/forgotPasswordKey', async function (req, res) {

    var data = {
        "_id": "5e15c066ecc56c06e0ae9167",
        "Adi": "ismail",
        "KullaniciAdi": "ismail",
        "Email": "ismail@ismail.com",
        "Password": "e72d40de4117de1514ce7ab4559310ee$e4510cf15b06d522618f63b04c3a76f3c73c991a6bc8695575e40a4d18a86569",
        "IndirimOrani": 5
    }

    try {
        const html = await ejs.renderFile('./Views/forgotPassword.ejs', {
            title: '[FORGOT_PASSWORD]',
            data: data,
            forgotPasswordKey: "xfsdfsdfsd"
        })
        var to = "ismail.cetin@experto.com.tr"
        var subject = "[FORGOT_PASSWORD]"
        command.sendMail(to, subject, html); 
        res.send(html)
    } catch (error) { 
        res.send("Mail Gönderilemedi")
    }
})
*/

const port = process.env.PORT;
const server = app.listen(port, function () {
    console.log(`Server (Açmak için ctrl + Left click) http://localhost:${port}`);
});