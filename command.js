const config = require('./config');
const jwt = require('jsonwebtoken');
var crypto = require('crypto');
const musteriModel = require('./models/musteriModel');
var path = require('path');
var fs = require('fs');
const userModel = require("./models2/user");
var XLSX = require('xlsx');
var nodemailer = require('nodemailer');
var companyMail = process.env.COMPANY_MAIL
var companyMailPass = process.env.COMPANY_MAIL_PASS
var transporter = nodemailer.createTransport({
    service: 'hotmail',
    auth: {
        user: companyMail,
        pass: companyMailPass
    }
});


//   var transport = nodemailer.createTransport("SMTP", {
//     host: "smtp-mail.outlook.com", // hostname
//     secureConnection: false, // TLS requires secureConnection to be false
//     port: 587, // port for secure SMTP
//     auth: {
//         user: companyMail,
//         pass: companyMailPass
//     },
//     tls: {
//         ciphers:'SSLv3'
//     }
// });


String.prototype.toObjectId = function () {
    var ObjectId = (require('mongoose').Types.ObjectId);
    return new ObjectId(this.toString());
};

Date.prototype.monthDays = function () {
    var d = new Date(this.getFullYear(), this.getMonth() + 1, 0);
    return d.getDate();
}

Date.prototype.ConvertMonth = function () {
    //sadece yıl ve ay bilgisini aya cevitrip gönderir
    var d = ((this.getFullYear() - 1970) * 12) + this.getMonth() + 1
    return d
}

Date.prototype.sonHaftaCumartesiVarMi = function () {
    var firstWeekday = new Date(this.getFullYear(), this.getMonth() + 1, 1).getDay();
    if (firstWeekday == 0 || firstWeekday == 6) { return true }
    else { return false }
}

Date.prototype.getWeekOfMonth = function () {
    var firstWeekday = new Date(this.getFullYear(), this.getMonth(), 1).getDay();
    var offsetDate = this.getDate() + firstWeekday - 1;
    return Math.ceil(offsetDate / 7);
}

Date.prototype.cumartesiVarMi = function () {
    var day = this.monthDays()
    var d = new Date(this.getFullYear(), this.getMonth(), day);
    var x = d.getWeekOfMonth();
    return x;

}


String.prototype.haftaSonuMu = function () {
    var str = this.trim();
    var arr = str.split(/[:./]/);
    var d = new Date(arr[2], arr[1] - 1, arr[0]);
    if (d.getDay() == 0 || d.getDay() == 6)//haftasonu
    {
        return true
    } else {
        return false
    }

}

String.prototype.saatiDakikayaCevir = function () {
    var str = this.trim();
    var arr = str.split(/[:./]/);
    return (parseInt(arr[0]) * 60) + parseInt(arr[1])
}


Date.prototype.TimezOlmadanGetir = function () {
    var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDay()));
    return d
}


String.prototype.convertTrDateToGlobalDate = function () {
    var str = this.trim();
    var arr = str.split(/[:./]/);
    arr[2] = "20" + arr[2]
    var d = new Date(Date.UTC(arr[2].substr(-4), arr[1] - 1, arr[0]));
    return d
}


function encode_base64(filename) {
    fs.readFile(path.join(__dirname + "/../", '/public/', filename), function (error, data) {
        if (error) {
            throw error;
        } else {
            var buf = Buffer.from(data);
            var base64 = buf.toString('base64');
            ////console.log('File convert to base64 string! ==> ' + base64);
            ////console.log('Base64 of ddr.jpg :' + base64);
            return base64;
        }
    });
}


function decode_base64(base64str, filename) {
    var buf = Buffer.from(base64str, 'base64');
    //console.log(__dirname);

    fs.writeFile(path.join(__dirname + "/../", 'public/', filename), buf, function (error) {
        if (error) {
            throw error;
        } else {
            // //console.log('File created from base64 string! ==> '+ __dirname+"/../",'public/',filename);
            return true;
        }
    });

}

function get_From_base64_To_xmlsData(base64str) {
    data = base64str.split(";base64,")[1];
    var read_opts = { type: 'base64' }
    var wb = XLSX.read(data, read_opts)
    return wb
}









let publicUrl = [
    "/api/auth/register",
    "/api/auth/login",
    "/api/user/list",
    "/api/user",
    "/api/musteri/list",
    "/api/auth/access-token",





    "/api/widgets/2", 
    "/api/musteri",
    "/api/auth",
    "/api/login",
    "/api/deneme",
    "/sendMail",
    "/api/access-token"
]

function TokenKontrol(req, res, next) {
    //public url kontrolü
    for (let index = 0; index < publicUrl.length; index++) {
        if (publicUrl[index] == req.path) {
            next();
            return;
        }
    }

    let token = req.headers['x-access-token'] || req.headers['authorization'] || req.headers['Authorization'];

    // Express headers are auto converted to lowercase
    if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }

    if (token) {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return req.returnTemplate([], "Token Geçerli Degil Lütfen Tekrar Giriş Yapınız.", config.error);

            } else {
                //userModel.find({ KullaniciAdi: decoded.KullaniciAdi }).then((user) => {
                userModel.find({ _id: decoded.id }).then((user) => {
                        if (user.length === 0) {
                        return req.returnTemplate([], "Token YOK Lütfen Giriş Yapınız", config.error);
                    }
                    req.TokenUser = user[0];
                    req.decoded = decoded;
                    next();
                }).catch(err => next(err));
            }
        });
    } else {
        return req.returnTemplate([], "Token YOK Lütfen Giriş Yapınız", config.error);

    }
}


function IzinKontrol(req, res, next) {
    //console.log('izin kontrol')
    if (req.decoded) {
    }
    next()
}




function databaseConnection() {
    // //console.log(db);


}


function ResHeader(req, res, next) {
    //console.log("ResHeader");
    // res.set({ 'content-type': 'application/json; charset=utf-8' });
    /*const head = {
     'Content-Range': `bytes ${start}-${end}/${fileSize}`,
     'Accept-Ranges': 'bytes',
     'Content-Length': chunksize,
     'Content-Type': 'video/mp4',
   }
   res.writeHead(200, head);
   */
    next()
}

function HashPassword(password) {
    const salt = crypto.randomBytes(16).toString('hex');
    const hash = crypto.pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex');
    return [salt, hash].join('$');
}

function VerifyHash(password, original) {
    const originalHash = original.split('$')[1];
    const salt = original.split('$')[0];
    const hash = crypto.pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex');
    return hash === originalHash
}









//kullanımı
//encode_base64('ddr.jpg');
//decode_base64('herhangi_base64_stringi','rane.jpg');



function ResponseModify(req, res, next) {
    req.returnTemplate = function returnJson(data, message, status = 200) {
        let responseTemplate = {
            status: status,
            data: data,
            messages: message
        }
        res.status(200).json(responseTemplate);

    }
    next();
}


function getToken(data) {
    const token = jwt.sign(data, config.secret);
    return token
}



function sendMail(to, subject, html = "") {
    if (!subject) { return [false, "Konu  yok"] }
    if (!validateEmail(to)) { return [false, "Mail adresini Kontrol Ediniz"] }
    var mailOptions = {
        from: companyMail,
        to: to,
        subject: subject,
        html: html
    };
 
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            //console.log(error);
            return [false, "Hata olustu :" + JSON.stringify(error)]
        } else {
            //console.log('Email sent: ' + info.response);
            return [true, ""]
        }
    });
}


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}




function jwtDecode(token) {

    if (token) {
        return jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return {} 
            }
            return decoded
        });
    } else {
        return {}
    }
}


module.exports = {
    TokenKontrol,
    IzinKontrol,
    databaseConnection,
    ResHeader,
    encode_base64,
    decode_base64,
    get_From_base64_To_xmlsData,
    ResponseModify,
    getToken,
    VerifyHash,
    HashPassword,
    sendMail,
    validateEmail,
    jwtDecode

}