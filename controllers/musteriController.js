var mongoose          = require('mongoose');
const musteriModel    = require("../models/musteriModel");
const teklifModel     = require("../models/teklifModel");
const desteklerModel  = require("../models/desteklerModel");
const userController  = require("../controllers/userController");

exports.potansiyelMusteriler = async (req, res) => {
  let result = await musteriModel.find(/*{MusteriStatus: 'Potansiyel'}*/);
  console.log('potansiyelMusteriler ', result)
  return res.status(200).json(result)
}

exports.aktifMusteriler = async (req, res) => {
  let result = await musteriModel.find({MusteriStatus: 'Aktif'});
  console.log('aktifMusteriler ', result)
  return res.status(200).json(result)
}

exports.pasifMusteriler = async (req, res) => {
  let result = await musteriModel.find({MusteriStatus: 'Pasif'});
  console.log('pasifMusteriler ', result)
  return res.status(200).json(result)
}

exports.sorunluMusteriler = async (req, res) => {
  let result = await musteriModel.find({MusteriStatus: 'Sorunlu'});
  console.log('sorunluMusteriler ', result)
  return res.status(200).json(result)
}










exports.list = async (req, res) => {
  if (req.query.musteriId) {
    let row = await musteriModel.findOne({ _id: req.query.musteriId, active: true })
      .populate('Temsilci')
      .populate('Kaynak')
      //.populate('Sektor')
      .populate('IlgiliKisi')
      //.populate('OSB')
      .populate('OncekiDestekler')
      .populate('Danisman')
      .populate('TeknikIlgiliKisi')
      .populate('MaliIlgiliKisi')
      .populate('YontimIlgiliKisi')

    console.log(row)
    return req.returnTemplate(row, "", 200)
  }
  let row = await musteriModel.find({ active: true })
    .populate('Temsilci')
    .populate('Kaynak')
    //.populate('Sektor')
    .populate('IlgiliKisi')
    //.populate('OSB')
    .populate('OncekiDestekler')
    .populate('Danisman')
    .populate('TeknikIlgiliKisi')
    .populate('MaliIlgiliKisi')
    .populate('YontimIlgiliKisi')
  return req.returnTemplate(row, "", 200)
};

exports.create = async (req, res) => {
  let payload = req.body;
  payload["Status"] = 0;
  //console.log('Musteri create')
  //console.log(payload)
  var new_musteriModel = new musteriModel(payload);
  let row = await new_musteriModel.save()
  return req.returnTemplate(row, "")
};

exports.getById = async (req, res) => {
  let row = await musteriModel.find({ _id: req.params.id });
  return req.returnTemplate(row, "")
};

exports.update = async (req, res) => {
  musteriModel.findOneAndUpdate(
    { _id: req.body._id },
    req.body,
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err, 500);
      }
      return req.returnTemplate(row, "")
    })
};

exports.delete = async (req, res) => {
  musteriModel.findOneAndUpdate({ _id: req.params.id }, { active: false }, { new: true }, (err, doc) => {
    if (err) {
      return req.returnTemplate([], err, 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir")
  })
};

exports.getTeklifler = async (req, res) => {

  var getAllAstUserList = [];
  getAllAstUserList.push(req.TokenUser.id)
  await userController.getAltUserId(getAllAstUserList, req.TokenUser.id)
  ////console.log(JSON.stringify(getAllAstUserList) )

  // getAllAstUserList=getAllAstUserList.map((item)=>{
  //   return  mongoose.Types.ObjectId(item) 
  //  //return `ObjectId("${item}")`  
  // }) 
  ////console.log( getAllAstUserList  )

  let row = await teklifModel.find({ Firma: req.params.id, MusteriTemsilcisi: { "$in": getAllAstUserList } })
    .populate('MusteriTemsilcisi')
    .populate('Urun')
    .populate('IlgiliKisi')
  // musteriModel.find({ _id: req.params.id }, (err, row) => {
  //   if (err) {
  //     res.status(500).send(err);
  //   }
  //   res.status(200).json(row);
  // });
  return req.returnTemplate(row, "")
};

exports.getOnayBekleyenTeklifler = async (req, res) => {
  var getAllAstUserList = [];
  await userController.getAltUserId(getAllAstUserList, req.TokenUser.id)

  let row = await teklifModel.find({ "TeklifDurumu": 1, MusteriTemsilcisi: { "$in": getAllAstUserList } })
    .populate('MusteriTemsilcisi')
    .populate('Urun')
    .populate('IlgiliKisi')
  return req.returnTemplate(row, "")
};

exports.getGecmisDestek = async (req, res) => {
  let row = await desteklerModel.find({ Musteri: req.params.id })
    .populate('Musteri')
  return req.returnTemplate(row, "")
};

exports.getWidgetMusteriAdaylariData = async (req, res) => {
  var response = {
    value: 75,
    ofTarget: 80,
    datasets: [
      {
        label: 'Conversion',
        data: [221, 80, 492, 471, 413, 344, 294]
      }
    ],
    labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    options: {
      spanGaps: false,
      legend: {
        display: false
      },
      maintainAspectRatio: false,
      layout: {
        padding: {
          top: 24,
          left: 16,
          right: 16,
          bottom: 16
        }
      },
      scales: {
        xAxes: [
          {
            display: false
          }
        ],
        yAxes: [
          {
            display: false,
            ticks: {
              min: 100,
              max: 500
            }
          }
        ]
      }
    }
  }
  return req.returnTemplate(response, "")
}