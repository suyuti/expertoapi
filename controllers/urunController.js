const urunModel = require("../models/urunModel");

exports.list = async (req, res) => {
  if (req.query.urunId) {
    let row = await urunModel.findOne({_id: req.query.urunId})
    return req.returnTemplate(row, "", 200)
  }
  else {
    let row = await urunModel.find()
    return req.returnTemplate(row, "",200)
  }
};

exports.create = async (req, res) => {
  let payload = req.body;
  var new_urunModel = new urunModel(payload);
  let row = await new_urunModel.save()
  //  new_urunModel.save((err, row) => {
  //     if (err) {
  //       res.status(500).send(err);
  //     }
  //     res.status(201).json(row);
  //   });  
  return req.returnTemplate(row, "")
};

exports.getById = async (req, res) => {
  let row = await newurunModel.save()
  // urunModel.find({ _id: req.params.id }, (err, row) => {
  //   if (err) {
  //     res.status(500).send(err);
  //   }
  //   res.status(200).json(row);
  // });
  return req.returnTemplate(row, "")
};

exports.update = async (req, res) => {
  urunModel.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, row) => {
      if (err) {
        return req.returnTemplate([], err,500);
      }
      return req.returnTemplate(row, "")
    }
  );
};

exports.delete = async (req, res) => {
  urunModel.remove({ _id: req.params.id }, (err, Category) => {
    if (err) { 
      return req.returnTemplate([], err , 404);
    }
    return req.returnTemplate([], "başarı ile silinmiştir") 
  });
};
