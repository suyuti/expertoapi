var express = require('express');
var router = express.Router();  

const AuthController            = require('./controllers2/auth')
const UserController            = require('./controllers2/user')
const MusteriController         = require('./controllers2/musteri')
const SektorController          = require('./controllers2/sektor')
const MusteriKaynakController   = require('./controllers2/musterikaynak')
const UrunController            = require('./controllers2/urun')
const GorusmeController         = require('./controllers2/gorusme')
const KisiController            = require('./controllers2/kisi')
const BoardController           = require('./controllers2/satisboard')
const SatisFirsatiController    = require('./controllers2/satisfirsati')
const TaskController            = require('./controllers2/task')
const RandevuController         = require('./controllers2/randevu')

// Auth
router.post      ('/auth',                                      AuthController.auth)
router.post      ('/auth/access-token',                         AuthController.access_token)
router.post      ('/auth/register',                             AuthController.register)

// User
router.get      ('/user',                                       UserController.list)     
router.post     ('/user',                                       UserController.create)  
router.get      ('/user/:id',                                   UserController.getById)        
router.put      ('/user/:id',                                   UserController.update)     
router.delete   ('/user/:id',                                   UserController.remove) 
router.post     ('/user/:id/activate',                          UserController.toggleActive)  

// Musteri
router.get      ('/satis/musteriler',                           MusteriController.list)     
router.post     ('/satis/musteri/save',                         MusteriController.create)  
router.get      ('/satis/musteri',                              MusteriController.getById)        
router.put      ('/satis/musteri/:id',                          MusteriController.update)     
router.delete   ('/musteri/:id',                                MusteriController.remove) 
router.post     ('/satis/musteri/:musteriId/adres',             MusteriController.adresEkle)  
router.get      ('/satis/musteri/:musteriId/adres',             MusteriController.adresList)  
router.delete   ('/satis/musteri/:musteriId/adres/:adresId',    MusteriController.adresSil)  

// Sektor
router.get      ('/sektor',                                     SektorController.list)     

// Musteri Kaynaklari
router.get      ('/musterikaynak',                              MusteriKaynakController.list)     

// Urun
router.get      ('/urun',                                       UrunController.list)     
router.get      ('/urun/:id',                                   UrunController.getById)     
router.post     ('/urun',                                       UrunController.create)     
router.put      ('/urun/:id',                                   UrunController.update)     
router.delete   ('/urun/:id',                                   UrunController.remove)     

// Gorusmeler
router.get      ('/gorusme/widgetdata/:userId',                         GorusmeController.widgetdata)      // musteri gorusme getir
router.get      ('/gorusme/:musteriId',                         GorusmeController.list)         // musteri ile yapilan gorusmeler
router.post     ('/gorusme/:musteriId',                         GorusmeController.create)       // musteri ile gorusme ekle
router.delete   ('/gorusme/:musteriId/:gid',                    GorusmeController.remove)       // musteri gorusme sil
router.get      ('/gorusme/:musteriId/:gid',                    GorusmeController.getById)      // musteri gorusme getir

// Kisiler
router.get      ('/kisi/',                                      KisiController.listAll)            // 
router.get      ('/kisi/:musteriId',                            KisiController.list)            // 
router.post     ('/kisi/:musteriId',                            KisiController.create)          // 
router.delete   ('/kisi/:musteriId/:kid',                       KisiController.remove)          // 
router.get      ('/kisi/:musteriId/:kid',                       KisiController.getById)         // 

// Boards
router.get      ('/satis/boards',                               BoardController.list)            // 
router.post     ('/satis/board/new',                            BoardController.create)            // 
router.get      ('/satis/board',                                BoardController.getById)            // 
router.get      ('/satis/board/satis',                          BoardController.getSatisBoard)            // 

// Satis firsati
router.get      ('/satis/satisfirsati',                         SatisFirsatiController.list)            // 
router.get      ('/satis/satisfirsati/:id',                     SatisFirsatiController.getById)            // 
router.post     ('/satis/satisfirsati',                         SatisFirsatiController.create)            // 
router.get      ('/satis/satisfirsati/widgetdata/:userId',      SatisFirsatiController.widgetdata)      // 

// Task
router.get      ('/taskApp/tasks',                              TaskController.list)                // 
router.put      ('/taskApp/tasks',                              TaskController.update)              // 
router.post     ('/taskApp/tasks',                              TaskController.create)              // 
router.delete   ('/taskApp/tasks',                              TaskController.remove)              // 
router.get      ('/taskApp/tasks/filters',                      TaskController.filters)             // 
router.get      ('/taskApp/tasks/folders',                      TaskController.folders)             // 
router.get      ('/taskApp/tasks/labels',                       TaskController.labels)              // 
router.post     ('/taskApp/tasks/folder',                       TaskController.setFolder)           // 
router.post     ('/taskApp/tasks/label',                        TaskController.toggleLabel)         // 
router.delete   ('/taskApp/tasks/todos',                        TaskController.deleteTodos)         // 

// Randevu
router.get      ('/randevu',                                    RandevuController.list)                // 
router.get      ('/randevu/:id',                                RandevuController.getById)                // 
router.post     ('/randevu',                                    RandevuController.create)                // 


module.exports = router;
