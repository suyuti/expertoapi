const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
  Adi           : {  type: String,  required: true } ,
  Tarih         : {  type: Date, required: true } ,
  Durum         : {  type: Number,  required: true } ,
  Musteri       : { type: Schema.Types.ObjectId, ref: 'musteriler' },
},{ 
  versionKey: false 
});


module.exports = mongoose.model("destekler", SchemaModel);



// Durum
// 1: devam ediyor
// 99:başarısız
// 200: başarılı

 