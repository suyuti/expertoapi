const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
  Musteri   : { type: Schema.Types.ObjectId, ref: 'musteriler' },
  Tarih     : {  type: String,  required: true } ,
  Kiminle   : { type: Schema.Types.ObjectId, ref: 'kisiler' },
  Kim       :  { type: Schema.Types.ObjectId, ref: 'user' },
  Konu       : {  type: String,  required: true }  ,
},{ 
  versionKey: false 
});


module.exports = mongoose.model("randevu", SchemaModel);
