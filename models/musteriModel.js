const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
  Status                : { type: Number,  required: true } ,
  active                : { type: Boolean, default: true},
  FirmaUnvani           : { type: String,  required: true } ,
  logo                  : { type: String } , 
  FirmaMarkasi          : { type: String} ,
  Temsilci              : { type: Schema.Types.ObjectId, ref: 'user' },
  Kaynak                : { type: Schema.Types.ObjectId, ref: 'musteriKaynaklari' },
  Sektor                : { type: Schema.Types.ObjectId, ref: 'sektorler' },
  IlgiliKisi            : { type: Schema.Types.ObjectId, ref: 'kisiler' },
  Adres                 : { type: String } ,
  il                    : { type: String  } ,
  ilce                  : { type: String } ,
  OSB                   : { type: String },
  //OSB                   : { type: Schema.Types.ObjectId, ref: 'osb' },
  Telefon               : { type: String } ,
  Web                   : { type: String } ,
  Mail                   : { type: String } ,
  CalisanSayisi         : { type: Number } ,
  TeknikPersonelSayisi  : { type: Number } ,
  MuhendisSayisi        : { type: Number } ,
  Ciro                  : { type: Number } ,
  KobiMi                : { type: Boolean } ,
  OncekiDestekler       : [{ type: Schema.Types.ObjectId, ref: 'destekler' }],
  Danisman              : { type: Schema.Types.ObjectId, ref: 'danisman' },
  VergiDairesi          : { type: String } ,
  VergiNo               : { type: String } ,
  EFaturaMi             : { type: Boolean } ,
  TeknikIlgiliKisi      : { type: Schema.Types.ObjectId, ref: 'kisiler' },
  MaliIlgiliKisi        : { type: Schema.Types.ObjectId, ref: 'kisiler' },
  YontimIlgiliKisi      : { type: Schema.Types.ObjectId, ref: 'kisiler' },
  MusteriStatus         : { type: String, enum: ['Aktif', 'Pasif', 'Potansiyel', 'Sorunlu'], default: 'Potansiyel'} 
},{ 
  versionKey: false 
});


module.exports = mongoose.model("musteriler", SchemaModel);
