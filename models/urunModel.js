const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({ 
  Adi                   : {  type: String, required: true },
  AktifMi               : {  type: Boolean, default: true },
  OnOdemeTutari         : {  type: Number },
  RaporBasiOdemeTutari  : {  type: Number },
  YuzdeTutari           : {  type: Number },
  SabitTutar            : {  type: Number },
  BasariTutari          : {  type: Number },
  KDVOrani              : {  type: Number },
},{ 
  versionKey: false 
});


module.exports = mongoose.model("urunler", SchemaModel);


// Dashboard
// 0 default
// 1 teknik
// 2 satış
// 3 mali
// 4 yönetim
