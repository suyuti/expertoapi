const mongoose = require("mongoose");
const Schema = mongoose.Schema;

  
var SchemaModel = new Schema({  
  IlCode              : { type: Schema.Types.ObjectId, ref: 'iller' },
  Name                : { type: String,  required: true } , 
},{ 
  versionKey: false 
});


module.exports = mongoose.model("ilceler", SchemaModel);
